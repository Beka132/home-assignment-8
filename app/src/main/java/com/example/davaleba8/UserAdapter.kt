package com.example.davaleba8

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.davaleba8.databinding.ItemNormalUserBinding
import com.example.davaleba8.databinding.ItemUserLayoutBinding

class UserAdapter(private val users:MutableList<ItemModelFirst>,private val userItemListener: UserItemListener):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object{
        private const val NORMAL_USER_VIEW_TYPE=1
        private const val USER_VIEW_TYPE=2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == NORMAL_USER_VIEW_TYPE) {
            NormalUserViewHolder(
                    ItemNormalUserBinding.inflate(
                            LayoutInflater.from(parent.context),
                            parent,
                            false
                    )
            )
        } else{
            UserViewHolder(
                    ItemUserLayoutBinding.inflate(
                            LayoutInflater.from(parent.context),
                            parent,
                            false
                    )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is UserViewHolder -> holder.bind()
            is NormalUserViewHolder-> holder.bind()
        }
    }

    override fun getItemCount()=users.size
    inner class UserViewHolder(private val binding: ItemUserLayoutBinding):RecyclerView.ViewHolder(binding.root){
        private lateinit var modelFirst: ItemModelFirst
        fun bind(){
            modelFirst=users[adapterPosition]
            binding.tvFirstName.setText(modelFirst.firstName)
            binding.tvLastName.setText(modelFirst.lastName)
            binding.root.setOnLongClickListener {
                userItemListener.userItemOnLongClick(adapterPosition)
                return@setOnLongClickListener true
            }
        }
    }

    inner class NormalUserViewHolder(private val binding: ItemNormalUserBinding):RecyclerView.ViewHolder(binding.root){
        private lateinit var modelFirst: ItemModelFirst
        fun bind(){
            modelFirst=users[adapterPosition]
            binding.ivProfile.setImageResource(modelFirst.profile?:R.drawable.man)
            binding.tvFirstName.setText(modelFirst.firstName)
            binding.tvLastName.setText(modelFirst.lastName)
            binding.root.setOnLongClickListener {
                userItemListener.userItemOnLongClick(adapterPosition)
                return@setOnLongClickListener true
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val model=users[position]
        return if (model.profile==null) USER_VIEW_TYPE else NORMAL_USER_VIEW_TYPE
    }
}