package com.example.davaleba8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.example.davaleba8.databinding.ActivityFirstBinding

class FirstActivity : AppCompatActivity() {
    private lateinit var binding:ActivityFirstBinding
    private val users= mutableListOf<ItemModelFirst>()
    private lateinit var adapter:UserAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityFirstBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initRecyclerView()
    }

    private fun initRecyclerView(){
        setData()
        adapter= UserAdapter(users,object :UserItemListener{
            override fun userItemOnLongClick(position: Int) {
                users.removeAt(position)
                adapter.notifyItemRemoved(position)
            }

        })
        binding.recyclerView.layoutManager=GridLayoutManager(this,2)
        binding.recyclerView.adapter=adapter

        binding.include.ivExit.setOnClickListener {
            onDestroy()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun setData(){
        users.add(ItemModelFirst("Abraham","Lincoln",R.drawable.abraham_lincoln))
        users.add(ItemModelFirst("Beka","Dvali"))
        users.add(ItemModelFirst("Christopher","Columbus",R.drawable.christopher_columbus))
        users.add(ItemModelFirst("Beka","Dvali"))
        users.add(ItemModelFirst("Julius","Caesar",R.drawable.man))
        users.add(ItemModelFirst("Beka","Dvali"))
        users.add(ItemModelFirst("George","Washington",R.drawable.george_washington))
        users.add(ItemModelFirst("Beka","Dvali",R.drawable.man))
        users.add(ItemModelFirst("Marilyn","Monroe",R.drawable.marilyn_monroe))
    }
}